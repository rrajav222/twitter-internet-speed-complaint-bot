from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from time import sleep
from datetime import time
import datetime as dt


chrome_driver_path = CHROME DRIVER PATH #Example : "C:/Users/USER/Desktop/Python/chromedriver.exe"
TWITTER_USERNAME = YOUR TWITTER USERNAME
TWITTER_PASSWORD = YOUR TWITTER_PASSWORD


class InternetSpeedTwitterBot:
    def __init__(self):
        self.driver = webdriver.Chrome(executable_path=chrome_driver_path)
        self.delay = 60 # Time limit to test net speed in seconds 
        self.installed_package = 40 
        self.net_unit = "Mbps"

    def get_internet_speed(self):
        self.driver.get("http://www.fast.com")
        try:
            myElem = WebDriverWait(self.driver, self.delay).until(EC.presence_of_element_located((By.CLASS_NAME, 'succeeded')))
            print("Page is ready!")
        except TimeoutException:
            print("Loading took too much time!")
            return None
        else:
            speed = self.driver.find_element_by_css_selector(".speed-results-container.succeeded")
            unit = self.driver.find_element_by_css_selector(".speed-units-container.succeeded")
            return float(speed.text),unit.text
    
    def tweet_at_provider(self,net_speed,net_unit):
        self.driver.get("http://www.twitter.com")
        sleep(2)
        login_button = self.driver.find_element_by_xpath('//*[@id="react-root"]/div/div/div/main/div/div/div/div[1]/div/div[3]/a[2]')
        login_button.click()
        sleep(2)
        username= self.driver.find_element_by_xpath('//*[@id="react-root"]/div/div/div[2]/main/div/div/div[2]/form/div/div[1]/label/div/div[2]/div/input')
        username.send_keys(TWITTER_USERNAME)
        password = self.driver.find_element_by_xpath('//*[@id="react-root"]/div/div/div[2]/main/div/div/div[2]/form/div/div[2]/label/div/div[2]/div/input')
        password.send_keys(TWITTER_PASSWORD)
        password.send_keys(Keys.ENTER)

        sleep(2)
        tweet_field = self.driver.find_element_by_xpath('//*[@id="react-root"]/div/div/div[2]/main/div/div/div/div[1]/div/div[2]/div/div[2]/div[1]/div/div/div/div[2]/div[1]/div/div/div/div/div/div/div/div/label/div[1]/div/div/div/div/div[2]/div/div/div/div')
        current_time = dt.datetime.now().strftime("%m/%d/%Y, %I:%M%p")
        tweet_field.send_keys(f"Vianet Internet Speed is {net_speed} {net_unit} on {current_time}. Installed package is {self.installed_package} {self.net_unit}")
        tweet_button = self.driver.find_element_by_xpath('//*[@id="react-root"]/div/div/div[2]/main/div/div/div/div[1]/div/div[2]/div/div[2]/div[1]/div/div/div/div[2]/div[3]/div/div/div[2]/div[3]')
        tweet_button.click()
        sleep(3)

bot = InternetSpeedTwitterBot()
net_speed,net_unit = bot.get_internet_speed()
print(net_speed,net_unit)

if net_unit == "Kbps":
    actual_speed = net_speed/1000
elif net_unit == "Mbps":
    actual_speed = net_speed

if actual_speed < bot.installed_package:
        tweet = bot.tweet_at_provider(net_speed, net_unit)
else:
    print("Net is fast enough on",dt.datetime.now().strftime("%m/%d/%Y, %I:%M%p"))

bot.driver.quit()